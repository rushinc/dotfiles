#! /bin/sh
sleep 10

# Applications
alacritty &
alacritty &
alacritty &
alacritty --class=ranger,ranger -e ranger &
firefox &
zotero &
alacritty --class=ncmpcpp,ncmpcpp -e ncmpcpp &
if [ "$HOSTNAME" = archStation ]; then
	alacritty --class=vis,vis -e vis &
fi
syncthing-gtk &
keepassxc &

# Services
nm-applet &
redshift &
mpd-notification &
